import Vue from 'vue'
import Element from 'element-ui'
import {
    Loading
} from 'element-ui';
import {
    Storage
} from '../utils/index.js'

import '../styles/element-variables.scss'
let loadingInstance

function showLoading() {
    loadingInstance = Loading.service({
        lock: true,
        fullscreen: true,
        background: 'rgba(0,0,0,0.7)'
    });
}

function hideLoading() {
    loadingInstance.close();
}

Vue.prototype.$showLoading = showLoading;
Vue.prototype.$hideLoading = hideLoading;

Vue.use(Element, {
    size: Storage.getStorage('size') || 'medium',
})
