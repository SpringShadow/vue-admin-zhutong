import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import Layout from '@/layout'
//个人中心
import personCenterRouter from './modules/person'
// 用户管理
import userManageRouter from './modules/user'
// 任务管理
// import taskManageRouter from './modules/task'
// 绩效管理
import performanceManageRouter from './modules/performance'
// 培训管理
import trainingManageRouter from './modules/training'
// 系统管理
import systemManageRouter from './modules/system'

export const constantRoutes = [{
        path: '/redirect',
        component: Layout,
        hidden: true,
        children: [{
            path: '/redirect/:path(.*)',
            component: () => import('@/views/redirect/index')
        }]
    },
    {
        path: '/login',
        component: () => import('@/views/login/index'),
        hidden: true
    },
    {
        path: '/auth-redirect',
        component: () => import('@/views/login/auth-redirect'),
        hidden: true
    },
    {
        path: '/404',
        component: () => import('@/views/error-page/404'),
        hidden: true
    },
    {
        path: '/401',
        component: () => import('@/views/error-page/401'),
        hidden: true
    },
    {
        path: '/',
        component: Layout,
        redirect: '/dashboard',
        children: [{
            path: 'dashboard',
            component: () => import('@/views/dashboard/index'),
            name: '首页',
            meta: {
                title: '首页',
                icon: 'home',
                affix: true
            }
        }]
    },
]

export const asyncRoutes = [
    personCenterRouter,
    userManageRouter,
    performanceManageRouter,
    // taskManageRouter,
    trainingManageRouter,
    // systemManageRouter,
    {
        path: '*',
        redirect: '/404',
        hidden: true
    }
]

const createRouter = () => new Router({
    scrollBehavior: () => ({
        y: 0
    }),
    routes: constantRoutes
})

const router = createRouter()

export function resetRouter() {
    const newRouter = createRouter()
    router.matcher = newRouter.matcher // reset router
}

export default router
