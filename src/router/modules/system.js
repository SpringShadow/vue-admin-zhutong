import Layout from '@/layout'

const systemManageRouter = {
    path: '/systemManage',
    component: Layout,
    meta: {
        title: '系统管理',
        icon: 'documentation',
        roles: ['admin']
    },
    name: '系统管理',
    redirect: '/systemManage/menu',
    children: [
        {
            path: 'activeLog',
            component: () => import('@/views/systemManage/activeLog'),
            name: '活动日志',
            meta: {
                title: '活动日志',
                icon: 'edit',
            },
        },
        {
            path: 'handleLog',
            component: () => import('@/views/systemManage/handleLog'),
            name: '操作日志',
            meta: {
                title: '操作日志',
                icon: 'edit',
            },
        },
    ]
}

export default systemManageRouter
