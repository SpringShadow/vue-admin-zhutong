import Layout from '@/layout'

const performanceManageRouter = {
    path: '/performance',
    component: Layout,
    meta: {
        title: '绩效管理',
        icon: 'el-icon-bank-card',
        roles: ['admin']
    },
    name: '绩效管理',
    redirect: '/performance/targetList',
    children: [
        {
            path: 'targetList',
            component: () => import('@/views/performance/targetList'),
            name: '绩效指标',
            meta: {
                title: '绩效指标',
                icon: 'list',
            },
            children:[
                {
                   path: '/performance/newTarget',
                   component: () => import('@/views/performance/target'),
                   name: '指标创建',
                   meta: {
                       title: '指标创建',
                       icon: '',
                   },
                   hidden: true, 
                },
                {
                   path: '/performance/editTarget',
                   component: () => import('@/views/performance/target'),
                   name: '指标编辑',
                   meta: {
                       title: '指标编辑',
                       icon: '',
                   },
                   hidden: true, 
                },
                {
                   path: '/performance/viewTarget',
                   component: () => import('@/views/performance/target'),
                   name: '指标详情',
                   meta: {
                       title: '指标详情',
                       icon: '',
                   },
                   hidden: true, 
                },
            ]
        },
        {
            path: 'nodeList',
            component: () => import('@/views/performance/nodeList'),
            name: '审批节点',
            meta: {
                title: '审批节点',
                icon: 'tree',
            },
            children:[
                {
                   path: '/performance/newNode',
                   component: () => import('@/views/performance/node'),
                   name: '节点创建',
                   meta: {
                       title: '节点创建',
                       icon: '',
                   },
                   hidden: true, 
                },
                {
                   path: '/performance/editNode',
                   component: () => import('@/views/performance/node'),
                   name: '节点编辑',
                   meta: {
                       title: '节点编辑',
                       icon: '',
                   },
                   hidden: true, 
                },
                {
                   path: '/performance/viewNode',
                   component: () => import('@/views/performance/node'),
                   name: '节点详情',
                   meta: {
                       title: '节点详情',
                       icon: '',
                   },
                   hidden: true, 
                },
            ]
        },
        {
            path: 'approvalList',
            component: () => import('@/views/performance/approvalList'),
            name: '绩效审批',
            meta: {
                title: '绩效审批',
                icon: 'el-icon-s-custom',
            },
            children:[
                {
                   path: '/performance/newApproval',
                   component: () => import('@/views/performance/approval'),
                   name: '绩效创建',
                   meta: {
                       title: '绩效创建',
                       icon: '',
                   },
                   hidden: true, 
                },
                {
                   path: '/performance/editApproval',
                   component: () => import('@/views/performance/approval'),
                   name: '绩效编辑',
                   meta: {
                       title: '绩效编辑',
                       icon: '',
                   },
                   hidden: true, 
                },
            ]
        },
        {
            path: 'reportList',
            component: () => import('@/views/performance/reportList'),
            name: '绩效报表',
            meta: {
                title: '绩效报表',
                icon: 'el-icon-s-marketing',
            },
            children:[
                {
                   path: '/performance/report',
                   component: () => import('@/views/performance/report'),
                   name: '绩效详情',
                   meta: {
                       title: '绩效详情',
                       icon: 'el-icon-s-marketing',
                   },
                   hidden: true, 
                },
            ]
      
            // 
        },
    ]
}

export default performanceManageRouter
