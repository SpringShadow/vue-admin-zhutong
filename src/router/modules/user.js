import Layout from '@/layout'

const userManageRouter = {
    path: '/userManage',
    component: Layout,
    meta: {
        title: '用户管理',
        icon: 'peoples',
        roles: ['admin']
    },
    name: '用户管理',
    redirect: '/userManage/userList',
    children: [
        {
            path: '/userManage/userList',
            component: () => import('@/views/userManage/userList'),
            name: '用户',
            meta: {
                title: '用户',
                icon: 'peoples',
            },
            children:[
                {
                   path: '/userManage/newUser',
                   component: () => import('@/views/userManage/user'),
                   name: '用户创建',
                   meta: {
                       title: '用户创建',
                       icon: '',
                   },
                   hidden: true, 
                },
                {
                   path: '/userManage/editUser',
                   component: () => import('@/views/userManage/user'),
                   name: '用户编辑',
                   meta: {
                       title: '用户编辑',
                       icon: '',
                   },
                   hidden: true, 
                },
                {
                   path: '/userManage/viewUser',
                   component: () => import('@/views/userManage/user'),
                   name: '用户详情',
                   meta: {
                       title: '用户详情',
                       icon: '',
                   },
                   hidden: true, 
                },
            ]
        },
        {
            path: 'roleList',
            component: () => import('@/views/userManage/roleList'),
            name: '角色',
            meta: {
                title: '角色',
                icon: 'user',
            },
            children:[
                {
                   path: '/userManage/newRole',
                   component: () => import('@/views/userManage/role'),
                   name: '角色创建',
                   meta: {
                       title: '角色创建',
                       icon: '',
                   },
                   hidden: true, 
                },
                {
                   path: '/userManage/editRole',
                   component: () => import('@/views/userManage/role'),
                   name: '角色编辑',
                   meta: {
                       title: '角色编辑',
                       icon: '',
                   },
                   hidden: true, 
                },
                {
                   path: '/userManage/viewRole',
                   component: () => import('@/views/userManage/role'),
                   name: '角色详情',
                   meta: {
                       title: '角色详情',
                       icon: '',
                   },
                   hidden: true, 
                },
            ]
        },
    ]
}

export default userManageRouter
