import Layout from '@/layout'

const taskManageRouter = {
    path: '/taskManage',
    component: Layout,
    meta: {
        title: '任务管理',
        icon: 'el-icon-s-data',
        roles: ['admin']
    },
    name: '任务管理',
    redirect: '/taskManage/taskList',
    children: [
        {
            path: 'taskList',
            component: () => import('@/views/taskManage/taskList'),
            name: '任务列表',
            meta: {
                title: '任务列表',
                icon: 'el-icon-data-board',
            },
        },
        {
            path: 'review',
            component: () => import('@/views/taskManage/review'),
            name: 'Review',
            meta: {
                title: 'Review',
                icon: 'el-icon-wind-power',
            },
        },
    ]
}

export default taskManageRouter
