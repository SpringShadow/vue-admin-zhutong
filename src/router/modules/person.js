import Layout from '@/layout'

const personCenterRouter = {
    path: '/personCenter',
    component: Layout,
    meta: {
        title: '个人中心',
        icon: 'el-icon-s-custom',
    },
    name: '个人中心',
    redirect: '/personCenter/myMessage',
    children: [
        {
            path: 'myProfile',
            component: () => import('@/views/userManage/user'),
            name: '我的资料',
            meta: {
                title: '我的资料',
                icon: 'el-icon-user-solid',
            },
        },
        {
            path: 'myPerformance',
            component: () => import('@/views/personCenter/myPerformance'),
            name: '我的绩效',
            meta: {
                title: '我的绩效',
                icon: 'el-icon-wallet',
            },
          children:[
            {
              path: '/myPerformance/view',
              component: () => import('@/views/personCenter/viewMyPerforance'),
              name: '查看',
              meta: {
                title: '查看',
                icon: '',
              },
              hidden: true,
            },
          ]
        },
        {
            path: 'myTodo',
            component: () => import('@/views/personCenter/myTodo'),
            name: '我的待办',
            meta: {
                title: '我的待办',
                icon: 'el-icon-s-order',
            },
            children:[
                {
                   path: '/personCenter/view',
                   component: () => import('@/views/personCenter/viewMyPerforance'),
                   name: '待办详情',
                   meta: {
                       title: '查看',
                       icon: '',
                   },
                   hidden: true, 
                },
                {
                   path: '/personCenter/entry',
                   component: () => import('@/views/personCenter/viewMyPerforance'),
                   name: '绩效录入',
                   meta: {
                       title: '录入',
                       icon: '',
                   },
                   hidden: true, 
                },
                {
                   path: '/personCenter/score',
                   component: () => import('@/views/personCenter/viewMyPerforance'),
                   name: '绩效评分',
                   meta: {
                       title: '评分',
                       icon: '',
                   },
                   hidden: true, 
                },
                
            ]
        },
        // {
        //     path: 'myMessage',
        //     component: () => import('@/views/personCenter/myMessage'),
        //     name: '我的消息',
        //     meta: {
        //         title: '我的消息',
        //         icon: 'el-icon-message-solid',
        //     },
        // },
    ]
}

export default personCenterRouter
