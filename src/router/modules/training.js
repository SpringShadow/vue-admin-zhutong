import Layout from '@/layout'

const trainingManageRouter = {
    path: '/trainingManage',
    component: Layout,
    meta: {
        title: '培训管理',
        icon: 'el-icon-school',
        roles: ['admin'],
    },
    name: '培训管理',
    redirect: '/trainingManage/material',
    children: [
        {
            path: 'material',
            component: () => import('@/views/trainingManage/material'),
            name: '培训资料',
            meta: {
                title: '培训资料',
                icon: 'el-icon-document',
            },
        },
        {
            path: 'courseList',
            component: () => import('@/views/trainingManage/courseList'),
            name: '培训课程',
            meta: {
                title: '培训课程',
                icon: 'el-icon-notebook-1',
            },
            children:[
                {
                   path: '/trainingManage/newCourse',
                   component: () => import('@/views/trainingManage/course'),
                   name: '课程创建',
                   meta: {
                       title: '课程创建',
                       icon: '',
                   },
                   hidden: true, 
                },
            ]
        },
    ]
}

export default trainingManageRouter
