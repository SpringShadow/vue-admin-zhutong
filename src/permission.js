import router from './router'
import store from './store'

import {
    Message
} from 'element-ui'

import NProgress from './plugins/nprogress.js'

import {
    getToken
} from '@/utils/auth'

import getPageTitle from '@/utils/get-page-title'

const whiteList = ['/login', '/auth-redirect']


router.beforeEach(async (to, from, next) => {
    // 显示进度条
    NProgress.start()

    document.title = getPageTitle(to.meta.title)

    const hasToken = getToken()
    if (hasToken) {
        // 如果有token 并且当前页面路径是登录页面  则到首页去 
        if (to.path === '/login') {
            next({
                path: '/'
            })
            NProgress.done()
        } else {
            // 获取用户权限
            const hasRoles = store.getters.roles && store.getters.roles.length > 0
            // 如果有权限 则直接进入页面
            if (hasRoles) {
                next()
            } else {
                try {
                    // 第一次进入根据token去获取用户的权限
                    const {
                        roles
                    } = await store.dispatch('user/getInfo')
                    
                    // 根据用户权限 生成动态路由
                    const accessRoutes = await store.dispatch('permission/generateRoutes', roles)
                    // 动态添加路由
                    router.addRoutes(accessRoutes)

                    next({ ...to,
                        replace: true
                    })
                } catch (error) {
                    await store.dispatch('user/resetToken')
                    Message.error(error || 'Has Error')
                    next(`/login?redirect=${to.path}`)
                    NProgress.done()
                }
            }
        }
    } else {
        // 如果是白名单里的页面 直接通过
        if (whiteList.includes(to.path)) {
            next()
        } else {
            // 否则到登录页面 然后带上该页面的地址 登录成功之后 再重定向回原来的页面
            next(`/login?redirect=${to.path}`)
            NProgress.done()
        }
    }
})

router.afterEach(() => {
    NProgress.done()
})
