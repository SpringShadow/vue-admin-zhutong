export const permissions = [{
        id: 1,
        label: '个人中心',
        path: '/personCenter',
        children: [
            {
                id: 6,
                path: 'myProfile',
                label: '我的资料'
            },
            {
                id: 7,
                path: 'myPerformance',
                label: '我的绩效'
            },
            {
                id: 8,
                path: 'myTodo',
                label: '我的待办'
            },
            {
                id: 9,
                path: 'myMessage',
                label: '我的消息'
            },
        ]
    },
    {
        id: 2,
        label: '用户管理',
        path: '/userManage',
        children: [{
                id: 10,
                path: 'userList',
                label: '用户'
            },
            {
                id: 11,
                path: 'roleList',
                label: '角色'
            }
        ]
    },
    {
        id: 3,
        label: '绩效管理',
        path: '/performance',
        children: [{
                id: 12,
                path: 'targetList',
                label: '绩效指标'
            },
            {
                id: 13,
                path: 'nodeList',
                label: '审批节点'
            },
            {
                id: 14,
                path: 'approvalList',
                label: '绩效审批'
            },
            {
                id: 15,
                path: 'reportList',
                label: '绩效报表'
            }
        ]
    },
    {
        id: 4,
        label: '任务管理',
        path: '/taskManage',
        children: [{
                id: 16,
                path: 'taskList',
                label: '任务列表'
            },
            {
                id: 17,
                path: 'review',
                label: 'Review'
            }
        ]
    },
 
    {
        id: 5,
        label: '培训管理',
        path: '/trainingManage',
        children: [{
                id: 18,
                path: 'material',
                label: '培训资料'
            },
            {
                id: 29,
                path: 'course',
                label: '培训课程'
            }
        ]
    },
    // {
    //     id: 6,
    //     label: '系统管理',
    //     path: '/systemManage',
    //     children: [{
    //             id: 21,
    //             path: 'menu',
    //             label: '菜单'
    //         },
    //         {
    //             id: 22,
    //             path: 'activeLog',
    //             label: '活动日志'
    //         },
    //         {
    //             id: 23,
    //             path: 'handleLog',
    //             label: '操作日志'
    //         }
    //     ]
    // }
]
