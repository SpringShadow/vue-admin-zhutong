import {Storage} from './index.js'
import {
    TokenKey
} from '../config/index.js'

export function getToken() {
    return Storage.getStorage(TokenKey)
}

export function setToken(token,expires_in) {
    return Storage.setStorage(TokenKey,token,expires_in)
}

export function removeToken() {
    return Storage.removeStorage(TokenKey)
    
}



