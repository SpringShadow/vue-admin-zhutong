import request from '@/utils/request'

// 上传文件
export function getFilesList(query) {
    return request({
        url: '/files',
        method: 'get',
        params: query
    })
}
// 上传文件
export function uploadFiles(data) {
    return request({
        url: '/files',
        method: 'post',
        data
    })
}
// 删除文件
export function deleteFile(data) {
    return request({
        url: '/files',
        method: 'delete',
        data
    })
}
// 移动文件
export function moveFile(data) {
    return request({
        url: '/files/move',
        method: 'post',
        data
    })
}
// 复制文件
export function copyFiles(data) {
    return request({
        url: '/files/copy',
        method: 'post',
        data
    })
}
// 下载文件
export function downloadFiles(path) {
    return request({
        url: `/files/download?path=${path}`,
        method: 'post',
    })
}
// 新增目录
export function createDirectory(data) {
    return request({
        url: '/directories',
        method: 'post',
        data
    })
}
// 删除目录
export function deleteDirectory(data) {
    return request({
        url: '/directories',
        method: 'delete',
        data
    })
}


