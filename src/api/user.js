import request from '@/utils/request'
// 获取员工列表
export function getSelectStaffsList() {
    return request({
        url: '/users/search',
        method: 'get',
    })
}

// 获取用户列表
export function getUserList(query) {
    return request({
        url: '/users',
        method: 'get',
        params: query
    })
}
// 获取用户详情
export function getUserDetail(userid) {
    return request({
        url: `/users/${userid}`,
        method: 'get',
    })
}
// 新增用户
export function newUser(data) {
    return request({
        url: '/users',
        method: 'post',
        data
    })
}
// 编辑用户
export function editUser(userid,data) {
    return request({
        url: `/users/${userid}`,
        method: 'put',
        data
    })
}


