import request from '@/utils/request'

export function login(data) {
    return request({
        url: '/authorization',
        method: 'post',
        data
    })
}

export function getInfo() {
    return request({
        url: '/authorization',
        method: 'get',
    })
}

export function logout() {
    return request({
        url: '/authorization',
        method: 'delete'
    })
}
