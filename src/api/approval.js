import request from '@/utils/request'

// 获取绩效指标列表
export function getDimensionsList() {
    return request({
        url: '/indicators/search',
        method: 'get',
    })
}
// 获取绩效审批列表
export function getApprovalList(query) {
    return request({
        url: '/performances',
        method: 'get',
        params: query
    })
}
// 重置审批
export function resetApproval(approvalId,data) {
    return request({
        url: `/performances/reset/${approvalId}`,
        method: 'patch',
        data
    })
}
// 发起审批
export function startApproval(approvalId) {
    return request({
        url: `/performances/start/${approvalId}`,
        method: 'patch',
    })
}
// 删除审批
export function deleteApproval(approvalId) {
    return request({
        url: `/performances/${approvalId}`,
        method: 'delete',
    })
}
// 复制审批
export function copyApproval(approvalId) {
    return request({
        url: `/performances/copy/${approvalId}`,
        method: 'post',
    })
}
// 编辑审批
export function editApproval(approvalId,data) {
    return request({
        url: `/performances/${approvalId}`,
        method: 'put',
        data
    })
}
// 获取审批详情
export function getApprovalDetail(approvalId) {
    return request({
        url: `/performances/${approvalId}`,
        method: 'get',
    })
}
// 新增审批
export function addApproval(data) {
    return request({
        url: `/performances`,
        method: 'post',
        data
    })
}
