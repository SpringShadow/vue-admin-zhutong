import request from '@/utils/request'

// 获取维度列表
export function getDimensionsList() {
    return request({
        url: '/indicators/dimensions',
        method: 'get',
    })
}

// 获取绩效指标列表
export function getIndicatorsList(query) {
    return request({
        url: '/indicators',
        method: 'get',
        params: query
    })
}

// 获取绩效指标详情
export function getIndicatorsDetail(targetId) {
    return request({
        url: `/indicators/${targetId}`,
        method: 'get'
    })
}
// 新增绩效指标
export function addIndicator(data) {
    return request({
        url: `/indicators`,
        method: 'post',
        data
    })
}

// 编辑绩效指标
export function editIndicator(targetId, data) {
    return request({
        url: `/indicators/${targetId}`,
        method: 'put',
        data
    })
}
// 删除绩效指标
export function deleteIndicator(targetId) {
    return request({
        url: `/indicators/${targetId}`,
        method: 'delete',
    })
}
