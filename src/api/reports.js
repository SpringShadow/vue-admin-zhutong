import request from '@/utils/request'

// 获取绩效列表
export function getReportsList(query) {
    return request({
        url: '/reports',
        method: 'get',
        params: query
    })
}

// 获取报表详情
export function getReportDetail(reportId) {
    return request({
        url: `/reports/${reportId}`,
        method: 'get',
    })
}
// 新增审批
export function addApproval(data) {
    return request({
        url: `/performances`,
        method: 'post',
        data
    })
}
// 员工确认
export function confirmReports(id) {
    return request({
        url: `/reports/confirm/${id}`,
        method: 'put',
    })
}
// 录入数据
export function inputData(id,data) {
    return request({
        url: `/reports/input/${id}`,
        method: 'put',
        data
    })
}
// 自评、经理评定
export function selfEvaluate(id,data) {
    return request({
        url: `/reports/score/${id}`,
        method: 'put',
        data
    })
}






