import request from '@/utils/request'
// 我的绩效
export function getMyPerformance() {
    return request({
        url: '/my/reports',
        method: 'get',
    })
}
// 我的待办
export function getMyTodo() {
    return request({
        url: '/my/todo',
        method: 'get',
    })
}




