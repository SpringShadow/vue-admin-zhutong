import request from '@/utils/request'

export function uploadAvatar(data) {
    return request({
        url: '/media',
        method: 'post',
        data
    })
}



