import request from '@/utils/request'

// 获取绩效节点列表
export function getNodeTypeList() {
    return request({
        url: '/nodes/types',
        method: 'get',
    })
}

export function getNodeAll() {
    return request({
        url: '/nodes/search',
        method: 'get',
    })
}

// 获取绩效节点列表
export function getNodesList() {
    return request({
        url: '/nodes',
        method: 'get',
    })
}

// 获取绩效节点详情
export function getNodesDetail(nodeId) {
    return request({
        url: `/nodes/${nodeId}`,
        method: 'get'
    })
}
// 新增绩效节点
export function addNode(data) {
    return request({
        url: `/nodes`,
        method: 'post',
        data
    })
}

// 编辑绩效节点
export function editNode(nodeId, data) {
    return request({
        url: `/nodes/${nodeId}`,
        method: 'put',
        data
    })
}
// 删除绩效节点
export function deleteNode(nodeId) {
    return request({
        url: `/nodes/${nodeId}`,
        method: 'delete',
    })
}
