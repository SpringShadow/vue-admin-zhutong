import request from '@/utils/request'

// 新建用户的时候 获取角色列表
export function getSelectRoleList() {
    return request({
        url: '/roles/search',
        method: 'get'
    })
}

// 获取已经创建的角色列表
export function getRolesList(query) {
    return request({
        url: '/roles',
        method: 'get',
        params: query
    })
}
// 获取角色详情
export function getRoleDetail(roleId) {
    return request({
        url: `/roles/${roleId}`,
        method: 'get'
    })
}
// 创建角色
export function createRole(data) {
    return request({
        url: '/roles',
        method: 'post',
        data
    })
}
// 编辑角色
export function editRole(roleId,data) {
    return request({
        url: `/roles/${roleId}`,
        method: 'put',
        data
    })
}
// 删除角色
export function deleteRole(roleId) {
    return request({
        url:`/roles/${roleId}`,
        method: 'delete',
        
    })
}



