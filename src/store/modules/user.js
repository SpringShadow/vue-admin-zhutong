import {
    login,
    logout,
    getInfo
} from '@/api/authorization'
import {
    getToken,
    setToken,
    removeToken
} from '@/utils/auth'
import router, {
    resetRouter
} from '@/router'

const state = {
    token: getToken(),
    name: '',
    avatar: '',
    nickname: '',
    userid:'',
    introduction: '',
    roles: [],
    rolename: ''
}

const mutations = {
    SET_TOKEN: (state, token) => {
        state.token = token
    },
    SET_INTRODUCTION: (state, introduction) => {
        state.introduction = introduction
    },
    SET_NAME: (state, name) => {
        state.name = name
    },
    SET_AVATAR: (state, avatar) => {
        state.avatar = avatar
    },
    SET_NICKNAME: (state, nickname) => {
        state.nickname = nickname
    },
    SET_USERID: (state, userid) => {
        state.userid = userid
    },
    SET_ROLES: (state, roles) => {
        state.roles = roles
    },
    SET_ROLENAME: (state, rolename) => {
        state.rolename = rolename
    }
}

const actions = {
    login({
        commit
    }, userInfo) {
        const {
            name,
            password
        } = userInfo
        return new Promise((resolve, reject) => {
            login({
                name: name.trim(),
                password: password
            }).then(response => {
                const {
                    data
                } = response
                commit('SET_TOKEN', data.access_token)
                setToken(data.access_token, data.expires_in / 60)
                resolve()
            }).catch(error => {
                console.log(error);
                reject(error)
            })
        })
    },

    // get user info
    getInfo({
        commit,
        state
    }) {
        return new Promise((resolve, reject) => {

            getInfo(state.token).then(response => {
                let {
                    data
                } = response
                const {
                    id,
                    name,
                    avatar,
                    introduction,
                    nickname
                } = data
                let user = data.roles.list[0]
                if (user.id == 1 || user.id == 2) {
                    data.roles = ['admin']
                }else{
                    data.roles = ['']
                }
                commit('SET_ROLES', data.roles)
                commit('SET_ROLENAME', user.name)
                commit('SET_NAME', name)
                commit('SET_AVATAR', avatar)
                commit('SET_NICKNAME', nickname)
                commit('SET_USERID', id)
                resolve(data)
            }).catch(error => {
                reject(error)
            })
        })
    },

    // user logout
    logout({
        commit,
        state,
        dispatch
    }) {
        return new Promise((resolve, reject) => {
            // 测试
            commit('SET_TOKEN', '')
            commit('SET_ROLES', [])
            removeToken()
            resetRouter()

            dispatch('tagsView/delAllViews', null, {
                root: true
            })

            resolve()

            // logout(state.token).then(() => {
            //     commit('SET_TOKEN', '')
            //     commit('SET_ROLES', [])
            //     removeToken()
            //     resetRouter()

            //     dispatch('tagsView/delAllViews', null, {
            //         root: true
            //     })

            //     resolve()
            // }).catch(error => {
            //     reject(error)
            // })
        })
    },
    // remove token
    resetToken({
        commit
    }) {
        return new Promise(resolve => {
            commit('SET_TOKEN', '')
            commit('SET_ROLES', [])
            removeToken()
            resolve()
        })
    },
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}
