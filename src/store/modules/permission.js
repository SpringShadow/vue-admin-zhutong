import {
    asyncRoutes,
    constantRoutes
} from '@/router'

/**
 * 使用 meta.role 来确定用户是否有权限
 * @param roles
 * @param route
 */
function hasPermission(roles, route) {
    // 如果有meta 同时meta上有定义用户权限 那么就去判断这个权限能否访问对应的路由 
    // 否则的话 就是没定义 那么就默认是有权限的
    if (route.meta && route.meta.roles) {
        return roles.some(role => route.meta.roles.includes(role))
    } else {
        return true
    }
}

/**
 * 用递归的方式过滤路由权限
 * @param routes asyncRoutes
 * @param roles
 */
export function filterAsyncRoutes(routes, roles) {
    const res = []
    routes.forEach(route => {
        const tmp = { ...route
        }
        if (hasPermission(roles, tmp)) {
            if (tmp.children) {
                tmp.children = filterAsyncRoutes(tmp.children, roles)
            }
            res.push(tmp)
        }
    })
    return res
}

const state = {
    routes: [],
    addRoutes: []
}

const mutations = {
    SET_ROUTES: (state, routes) => {
        state.addRoutes = routes
        state.routes = constantRoutes.concat(routes)
    }
}

const actions = {
    generateRoutes({
        commit
    }, roles) {
        return new Promise(resolve => {
            // let permissions = [{
            //         path: '/personCenter',
            //         name: '个人中心',
            //         children: [{
            //                 path: 'message',
            //                 name: '我的消息'
            //             },
            //             {
            //                 path: 'todo',
            //                 name: '我的待办'
            //             },
            //         ]
            //     },
            //     {
            //         path: '/performance',
            //         name: '绩效管理',
            //         children: [{
            //                 path: 'target',
            //                 name: '绩效指标'
            //             },
            //             {
            //                 path: 'node',
            //                 name: '审批节点'
            //             },
            //             {
            //                 path: 'approval',
            //                 name: '绩效审批'
            //             },
            //         ]
            //     }
            // ]
            // let routes = getRoutes(asyncRoutes, permissions)
            let accessedRoutes = []
            if (roles.includes('admin')) {
                accessedRoutes = asyncRoutes || []
            } else {
                accessedRoutes = filterAsyncRoutes(asyncRoutes, roles)
            }
            commit('SET_ROUTES', accessedRoutes)
            resolve(accessedRoutes)
        })
    }
}

function getRoutes(asyncRoutes, permissions) {
    const res = [];
    asyncRoutes.forEach((route) => {
        const tmp = { ...route
        }
        if (includesPath(route.path, permissions)) {
            if (tmp.children) {
                tmp.children = getRoutes(tmp.children, getChildren(route.path, permissions))
            }
            res.push(tmp)
        }
    })
    return res
}

function includesPath(path, arr) {
    let include = false;
    arr.forEach((item) => {
        if (item.path === path) {
            include = true
        }
    })
    return include
}

function getChildren(path, arr) {
    let children = []
    arr.forEach((item) => {
        if (item.path === path && item.children) {
            children = item.children
        }
    })
    return children
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}
